class TermsController < ApplicationController
  before_action :set_term, only: [:show, :edit, :update, :destroy]

  # GET /terms
  # GET /terms.json
  def index
    @terms = Term.all
  end

  # GET /terms/1
  # GET /terms/1.json
  def show
  end

  # GET /terms/new
  def new
    @term = Term.new
      @classes = Aclass.all
        if params[:aclass_id].blank?
            @class_id = "" 
              
       else
             @class = Aclass.find_by_id(params[:aclass_id]) 
             @class_id = @class.id
             
       end
  end

  # GET /terms/1/edit
  def edit
    @aclass_id = Aclass.find_by(params[:aclass_id])
  end

  # POST /terms
  # POST /terms.json
  def create
       # params[:term][:start_date] = Date::strptime(params[:term][:start_date], "%m/%d/%Y")
       # params[:term][:end_date] = Date::strptime(params[:term][:end_date], "%m/%d/%Y")


    @term = Term.new(term_params)

    respond_to do |format|
      if @term.save
        format.html { redirect_to @term, notice: 'Term was successfully created.' }
        format.json { render :show, status: :created, location: @term }
      else
        format.html { render :new }
        format.json { render json: @term.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /terms/1
  # PATCH/PUT /terms/1.json
  def update
   

    @aclass_id = (params[:aclass_id])
    respond_to do |format|
      if @term.update(term_params)
        format.html { redirect_to @term, notice: 'Term was successfully updated.' }
        format.json { render :show, status: :ok, location: @term }
      else
        format.html { render :edit }
        format.json { render json: @term.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /terms/1
  # DELETE /terms/1.json
  def destroy
    @term.destroy
    respond_to do |format|
      format.html { redirect_to terms_url, notice: 'Term was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_term
      @term = Term.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def term_params
      params.require(:term).permit(:term, :start_date,:end_date, :aclass_id)
    end
end
