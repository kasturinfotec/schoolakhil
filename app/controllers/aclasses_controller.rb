class AclassesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_aclass, only: [:show, :edit, :update, :destroy]

  # GET /aclasses
  # GET /aclasses.json
  def index
    @aclasses = Aclass.all
  end

  # GET /aclasses/1
  # GET /aclasses/1.json
  def show
  end

  # GET /aclasses/new
  def new
    @aclass = Aclass.new
    @gradelevels=Gradelevel.all
    @instructors=Instructor.all
  end

  # GET /aclasses/1/edit
  def edit
  end

  # POST /aclasses
  # POST /aclasses.json
  def create
    @aclass = Aclass.new(aclass_params)

    respond_to do |format|
      if @aclass.save
        format.html { redirect_to "/aclasses", notice: 'Aclass was successfully created.' }

        format.json { render :show, status: :created, location: @aclass }
      else
        format.html { render :new }
        format.json { render json: @aclass.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /aclasses/1
  # PATCH/PUT /aclasses/1.json
  def update
    respond_to do |format|
      if @aclass.update(aclass_params)
        format.html { redirect_to @aclass, notice: 'Aclass was successfully updated.' }
        format.json { render :show, status: :ok, location: @aclass }
      else
        format.html { render :edit }
        format.json { render json: @aclass.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /aclasses/1
  # DELETE /aclasses/1.json
  def destroy
    @aclass.destroy
    respond_to do |format|
      format.html { redirect_to aclasses_url, notice: 'Aclass was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_aclass
      @aclass = Aclass.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def aclass_params
  params.require(:aclass).permit(:aclass, :gradelevel_id, :instructor_id ) 
    end
end
