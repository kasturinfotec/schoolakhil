class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


 
 before_filter :configure_permitted_parameters, if: :devise_controller?
 layout :admin_layout
  def after_sign_in_path_for(resource_or_scope)
        if current_user.role== 0
          "/"
        else
         "/instructors"
        end
    end

  def configure_permitted_parameters
   devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:full_name, :class_name, :phone, :email, :password, :password_confirmation) }
  end
def admin_layout
    
    layout1 = ""
    if current_user.nil?
      layout1 = "application"
    elsif current_user.role == 0

          layout1 = "application"
      else
          layout1 = "instructors"

      end
         layout1
  end
end
