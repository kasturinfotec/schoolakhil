class GradelevelsController < ApplicationController
    before_filter :authenticate_user!
  before_action :set_gradelevel, only: [:show, :edit, :update, :destroy]

  # GET /gradelevels
  # GET /gradelevels.json
  def index
    @gradelevels = Gradelevel.all
  end

  # GET /gradelevels/1
  # GET /gradelevels/1.json
  def show
  end

  # GET /gradelevels/new
  def new
    @gradelevel = Gradelevel.new
        @classes = Aclass.all
        @tests = []
        if !params[:aclass_id].blank?
         
             @class = Aclass.find_by_id(params[:aclass_id]) 
             @class_id = @class.id
             @terms  = @class.terms
             @term_id = (params[:term_id])
             @students =[]
             
       elsif !params[:term_id].blank?
          @term =Term.find_by_id(params[:term_id])
           @class = @term.aclass
            @class_id = @class.id
             @terms  = @class.terms
            @term_id = (params[:term_id])
             #@test_id = @gradelevel.test.id
            @tests = @term.tests
            @students =@class.students
          elsif !params[:test_id].blank?
          @test =Test.find_by_id(params[:test_id])
          @test_id=@test.id
          @term=@test.term
          @term_id = @term.id
           @class = @term.aclass
            @class_id = @class.id
             @terms  = @class.terms
            
             #@test_id = @gradelevel.test.id
            @tests = @term.tests
            @students =@class.students
      else 
            @class_id = "" 
            @classes = Aclass.all
            @terms = [] 
            @students = [] 
       end

      # @students =@class.students
  end

  # GET /gradelevels/1/edit
  def edit
     @classes = Aclass.all
        @tests = []
        @student_id = @gradelevel.student_id
    @term =@gradelevel.test.term
           @class = @term.aclass
            @class_id = @class.id
             @terms  = @class.terms
            @term_id = @term.id
            @test_id = @gradelevel.test.id
             @student_id=@gradelevel.student.id
            @tests = @term.tests
            @students =@class.students
      if !params[:aclass_id].blank?
         
             @class = Aclass.find_by_id(params[:aclass_id]) 
             @class_id = @class.id
             @terms  = @class.terms
             @term_id = (params[:term_id])
             @students =[]
             
       elsif !params[:term_id].blank?
          @term =Term.find_by_id(params[:term_id])
           @class = @term.aclass
            @class_id = @class.id
             @terms  = @class.terms
            @term_id = (params[:term_id])
             #@test_id = @gradelevel.test.id
            @tests = @term.tests
            @students =@class.students
          elsif !params[:test_id].blank?
          @test =Test.find_by_id(params[:test_id])
          @test_id=@test.id
          @term=@test.term
          @term_id = @term.id
           @class = @term.aclass
            @class_id = @class.id
             @terms  = @class.terms
            
             #@test_id = @gradelevel.test.id
            @tests = @term.tests
            @students =@class.students
          end
  end

  # POST /gradelevels
  # POST /gradelevels.json
  def create
    @gradelevel = Gradelevel.new(gradelevel_params)

    respond_to do |format|
      if @gradelevel.save
        format.html { redirect_to "/testdata", notice: 'Gradelevel was successfully created.' }
        format.json { render :show, status: :created, location: @gradelevel }
      else
        format.html { render :new }
        format.json { render json: @gradelevel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gradelevels/1
  # PATCH/PUT /gradelevels/1.json
  def update
    respond_to do |format|
      if @gradelevel.update(gradelevel_params)
        format.html { redirect_to @gradelevel, notice: 'Gradelevel was successfully updated.' }
        format.json { render :show, status: :ok, location: @gradelevel }
      else
        format.html { render :edit }
        format.json { render json: @gradelevel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gradelevels/1
  # DELETE /gradelevels/1.json
  def destroy
    @gradelevel.destroy
    respond_to do |format|
      format.html { redirect_to gradelevels_url, notice: 'Gradelevel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gradelevel
      @gradelevel = Gradelevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gradelevel_params
      params.require(:gradelevel).permit(:gradelevel, :term_id, :aclass_id, :test_id,:student_id, :grade_id)
    end
end