class HomeController < ApplicationController
  before_filter :authenticate_user!
  #before_filter :admin_authenticate

   
  def index

  end

  def search
    if !params[:id].blank?
      @students = Student.where(:id=>params[:id])
      render "student_id"
    elsif !params[:first_name].blank?
      @students = Student.where("first_name like '%#{params[:first_name]}%'")
      
    elsif !params[:tier_level].blank?
       @students = Student.where("tier_level like '%#{params[:tier_level]}%'")
     render "student_by_tier"
     else
       @students=[]
    end
	   #@student = Student.find_all_by(params[:first_name])
  end
  def student_id
    @student = Student.find_by(params[:id] )
    @students=[]
  end
  def student_by_tier
    #@student = Student.all
    @students=[]
  end
  def instructor_params
      params.require(:students).permit(:first_name,:student_id)
    end

	 
end
