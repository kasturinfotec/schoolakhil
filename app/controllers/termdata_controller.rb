class TermdataController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_termdatum, only: [:show, :edit, :update, :destroy]

  # GET /termdata
  # GET /termdata.json
  def index
    @termdata = Termdatum.all
  end

  # GET /termdata/1
  # GET /termdata/1.json
  def show
  end

  # GET /termdata/new
  def new
    @termdatum = Termdatum.new
  end

  # GET /termdata/1/edit
  def edit
     
  end

  # POST /termdata
  # POST /termdata.json
  def create
    @termdatum = Termdatum.new(termdatum_params)

    respond_to do |format|
      if @termdatum.save
        format.html { redirect_to @termdatum, notice: 'Termdatum was successfully created.' }
        format.json { render :show, status: :created, location: @termdatum }
      else
        format.html { render :new }
        format.json { render json: @termdatum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /termdata/1
  # PATCH/PUT /termdata/1.json
  def update
    respond_to do |format|
      if @termdatum.update(termdatum_params)
        format.html { redirect_to @termdatum, notice: 'Termdatum was successfully updated.' }
        format.json { render :show, status: :ok, location: @termdatum }
      else
        format.html { render :edit }
        format.json { render json: @termdatum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /termdata/1
  # DELETE /termdata/1.json
  def destroy
    @termdatum.destroy
    respond_to do |format|
      format.html { redirect_to termdata_url, notice: 'Termdatum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_termdatum
      @termdatum = Termdatum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def termdatum_params
      params[:termdatum]
    end
end
