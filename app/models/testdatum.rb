# == Schema Information
#
# Table name: testdata
#
#  id         :integer          not null, primary key
#  benchmark  :integer
#  date       :date
#  created_at :datetime
#  updated_at :datetime
#  student_id :integer
#  term_id    :integer
#



class Testdatum < ActiveRecord::Base

	has_one :test
	#########
	belongs_to :term
	has_many :students
	has_many :aclasses, through: :students
	belongs_to :term
end
