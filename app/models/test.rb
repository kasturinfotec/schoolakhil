# == Schema Information
#
# Table name: tests
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  term_id     :integer
#


class Test < ActiveRecord::Base
	belongs_to :term
	has_many :gradelevels
	##########
	#has_many :testdatum
	#belongs_to :term
end
