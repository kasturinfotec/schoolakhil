# == Schema Information
#
# Table name: grades
#
#  id            :integer          not null, primary key
#  grade         :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  gradelevel_id :integer
#

class Grade < ActiveRecord::Base
	has_many :gradelevel
end
