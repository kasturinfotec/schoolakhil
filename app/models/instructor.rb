# == Schema Information
#
# Table name: instructors
#
#  id          :integer          not null, primary key
#  first_name  :string(255)
#  middle_name :string(255)
#  last_name   :string(255)
#  email       :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  gender      :string(255)
#  aclass_id   :integer
#

class Instructor < ActiveRecord::Base
	has_one :aclass


	def full_name
		return first_name+" "+last_name	
		
	end
	
end
