# == Schema Information
#
# Table name: termdata
#
#  id         :integer          not null, primary key
#  term_id    :integer
#  student_id :integer
#  created_at :datetime
#  updated_at :datetime
#



class Termdatum < ActiveRecord::Base
	belongs_to :term
	has_many :aclasses, through: :students
end
