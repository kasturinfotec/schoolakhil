# == Schema Information
#
# Table name: aclasses
#
#  id            :integer          not null, primary key
#  gradelevel_id :integer
#  instructor_id :integer
#  created_at    :datetime
#  updated_at    :datetime
#  aclass        :string(255)
#





class Aclass < ActiveRecord::Base
	has_many :students
	has_many  :terms
	belongs_to :instructor
	############
	#belongs_to :gradelevel
	#has_many :termdatums, through: :students
end
