# == Schema Information
#
# Table name: gradelevels
#
#  id         :integer          not null, primary key
#  gradelevel :string(255)
#  aclass_id  :integer
#  created_at :datetime
#  updated_at :datetime
#  test_id    :integer
#  student_id :integer
#  grade_id   :integer
#  benchmark  :string(255)
#

class Gradelevel < ActiveRecord::Base
	belongs_to :grade
	belongs_to :student
	belongs_to :test
end
