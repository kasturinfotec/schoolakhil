# == Schema Information
#
# Table name: students
#
#  id            :integer          not null, primary key
#  first_name    :string(255)
#  middle_name   :string(255)
#  last_name     :string(255)
#  email         :string(255)
#  date_of_birth :date
#  tier_level    :string(255)
#  phone         :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  gender        :string(255)
#  aclass_id     :integer
#  term_id       :integer
#  testdata_id   :integer
#  testdatum_id  :integer
#


#

class Student < ActiveRecord::Base
	belongs_to :aclass
	#belongs_to :termdatum
	#has_many :tests
	#has_many :terms
	#belongs_to :testdata
	has_many :gradelevels

	######## valdation#############
	validates :aclass_id, presence: true
	#validates :phone, format: { with: /\d{3}-\d{3}-\d{4}/, message: "bad format" }


	def fullname
		return first_name+" "+middle_name+" "+last_name	
	end


end
