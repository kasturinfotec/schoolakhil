// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

//= require "jquery-1.10.2.min.js"
//= require "jquery.dataTables.min.js"
//= require "jquery-ui-1.8.12.min.js"
//= require "superfish.js"
//= require "bootstrap.min.js"
//= require "retina.min.js"
//= require "jquery.validate.js"
//= require "jquery.placeholder.js"
//= require "functions.js"
//= require "classie.js"
//= require "uisearch.js"
//= require "dataTables.bootstrap.js"
//= require "jquery.wizard.js"
//= require "jquery.icheck.js"
//= require "validate.js"
//= require "wizard_func.js"
= requier "jquery-ui.js"
= requier "jquery-1.10.2.js"
