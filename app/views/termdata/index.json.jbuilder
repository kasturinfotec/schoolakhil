json.array!(@termdata) do |termdatum|
  json.extract! termdatum, :id
  json.url termdatum_url(termdatum, format: :json)
end
