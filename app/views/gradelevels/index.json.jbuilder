json.array!(@gradelevels) do |gradelevel|
  json.extract! gradelevel, :id, :gradelevel
  json.url gradelevel_url(gradelevel, format: :json)
end
