json.array!(@aclasses) do |aclass|
  json.extract! aclass, :id
  json.url aclass_url(aclass, format: :json)
end
