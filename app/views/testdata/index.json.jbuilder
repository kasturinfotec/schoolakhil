json.array!(@testdata) do |testdatum|
  json.extract! testdatum, :id, :benchmark, :date
  json.url testdatum_url(testdatum, format: :json)
end
