json.array!(@students) do |student|
  json.extract! student, :id, :first_name, :middle_name, :last_name, :email, :date_of_birth, :tier_level, :phone, :gender
  json.url student_url(student, format: :json)
end
