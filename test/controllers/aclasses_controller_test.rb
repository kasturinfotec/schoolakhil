require 'test_helper'

class AclassesControllerTest < ActionController::TestCase
  setup do
    @aclass = aclasses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:aclasses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create aclass" do
    assert_difference('Aclass.count') do
      post :create, aclass: {  }
    end

    assert_redirected_to aclass_path(assigns(:aclass))
  end

  test "should show aclass" do
    get :show, id: @aclass
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @aclass
    assert_response :success
  end

  test "should update aclass" do
    patch :update, id: @aclass, aclass: {  }
    assert_redirected_to aclass_path(assigns(:aclass))
  end

  test "should destroy aclass" do
    assert_difference('Aclass.count', -1) do
      delete :destroy, id: @aclass
    end

    assert_redirected_to aclasses_path
  end
end
