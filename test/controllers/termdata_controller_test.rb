require 'test_helper'

class TermdataControllerTest < ActionController::TestCase
  setup do
    @termdatum = termdata(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:termdata)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create termdatum" do
    assert_difference('Termdatum.count') do
      post :create, termdatum: {  }
    end

    assert_redirected_to termdatum_path(assigns(:termdatum))
  end

  test "should show termdatum" do
    get :show, id: @termdatum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @termdatum
    assert_response :success
  end

  test "should update termdatum" do
    patch :update, id: @termdatum, termdatum: {  }
    assert_redirected_to termdatum_path(assigns(:termdatum))
  end

  test "should destroy termdatum" do
    assert_difference('Termdatum.count', -1) do
      delete :destroy, id: @termdatum
    end

    assert_redirected_to termdata_path
  end
end
