class CreateTestdata < ActiveRecord::Migration
  def change
    create_table :testdata do |t|
      t.integer :benchmark
      t.date :date

      t.timestamps
    end
  end
end
