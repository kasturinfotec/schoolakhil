class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :email
      t.date :date_of_birth
      t.string :tier_level
      t.string :phone

      t.timestamps
    end
  end
end
