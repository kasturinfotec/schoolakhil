
# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141104123744) do

  create_table "aclasses", force: true do |t|
    t.integer  "gradelevel_id"
    t.integer  "instructor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "aclass"
  end

  create_table "gradelevels", force: true do |t|
    t.string   "gradelevel"
    t.integer  "aclass_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "test_id"
    t.integer  "student_id"
    t.integer  "grade_id"
    t.string   "benchmark"
  end

  create_table "grades", force: true do |t|
    t.string   "grade"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "gradelevel_id"
  end

  create_table "instructors", force: true do |t|
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "gender"
    t.integer  "aclass_id"
  end

  create_table "students", force: true do |t|
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "email"
    t.date     "date_of_birth"
    t.string   "tier_level"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "gender"
    t.integer  "aclass_id"
    t.integer  "term_id"
    t.integer  "testdata_id"
    t.integer  "testdatum_id"
  end

  create_table "termdata", force: true do |t|
    t.integer  "term_id"
    t.integer  "student_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "terms", force: true do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.string   "term"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "aclass_id"
  end

  create_table "testdata", force: true do |t|
    t.integer  "benchmark"
    t.date     "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "student_id"
    t.integer  "term_id"
  end

  create_table "tests", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "term_id"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "full_name"
    t.string   "class_name"
    t.string   "phone"
    t.integer  "role",                   default: 1
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
>>>>>>> 460068ce70901b3e7fd13347ccc3e041078797a7
