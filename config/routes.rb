Rails.application.routes.draw do
  

  
        #devise_for :users, :controllers => { :sessions => "users/sessions" }
        root to: "home#index"
        devise_for :users

        resources :aclasses
        resources :testdata , :controller=>"gradelevels", :as=>:gradelevels
        #resources :testdata
        resources :termdata
        resources :tests
        resources :terms
        resources :instructors
        resources :students
        resources :grades


        get 'index' => 'home#index'
       # get 'faculty' => 'faculty#index'
=begin
        get 'students' => 'students#index'
        get 'instructors' => 'instructor#index_for_view'
        get 'term' => 'term#index'
        get 'termdeta' => 'termdeta#index'
        get 'test' => 'test#index'
        get 'testdata' => 'testdata#index'
        get 'class' => 'aclasses#index'
        get 'gradelevels' => 'gradelevels#index'
=end

        # get 'search'=> 'instructors#search'

        match '/search' => 'home#search' , :via => [:post , :get]
        match '/student_id' => 'home#student_id' , :via => [:post , :get]
        match '/student_by_tier' => 'home#student_by_tier' , :via => [:post , :get]
        # get 'student_name' => 'home#student_name'
        # get 'student_id' => 'home#student_id'
        # get 'benchmark' => 'home#benchmark'
        # get 'tier' => 'home#tier'
        # get 'result' => 'home#result'
        # get 'test_scores' => 'home#test_scores'



  #get 'home/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
